# MIB for IEEE Std 1588

## Description

This GitLab project contains published MIB file(s) for management of products that conform to IEEE Std 1588. IEEE Std 1588 is commonly known as Precision Time Protocol (PTP). Each file is considered a normative part of the corresponding published IEEE Standards Association project for IEEE Std 1588 (i.e., revision, amendment, or corrigendum). The identification of the corresponding IEEE-SA project is contained in each file's description.

## Copyright

Users of IEEE Std 1588 may freely reproduce the MIB files contained in this GitLab project, so that they can be used for their intended purpose.

All files contained within this GitLab project are considered to be intended as IEEE Contributions. All Contributions to IEEE standards development (whether for an individual or entity standard) shall meet the requirements outlined in the IEEE-SA Copyright Policy.

## File Naming

A file that uses the base name of the module (e.g., IEEE1588-MIB.mib) contains the most recently published module.

Past publications of each module are also available in this GitLab project. The file for a past publication is associated with a proper git tag.

## Contributing

As an individual contributor to the standard, please do not use pull requests, merges, or issues to contribute to this GitLab project. Those operations are used by IEEE 1588 Working Group membership in order to upload a publication (e.g., during IEEE-SA balloting).

To contribute as an individual, please join the IEEE 1588 Working Group:
https://sagroups.ieee.org/1588/how-to-join-p1588/

Once you are a member of the IEEE 1588 Working Group, you can use the group's policies and procedures in order to contribute to an IEEE-SA project for IEEE Std 1588. Clarifications and bugs for a MIB module are handled with a Maintenance procedure. New features for a MIB module are handled with a New Features procedure.

As an IEEE-SA project proceeds, multiple ballots are held in order to gather comments and reach consensus on changes. Each ballot uses a draft version of the corresponding MIB module. Draft MIB modules are not available in this GitLab project. The location of a draft MIB module is identified in the balloting instructions (i.e., within the IEEE 1588 Working Group).
